# Created by Roy WAN at 20:22 29/12/2019 using PyCharm

import datetime
from time import sleep


def get_time():
    print('   %Y-%m-%d %H:%M:%S')
    n = 180
    while n > 0:
        print("\r", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), end='')
        sleep(1)
        n -= 1


get_time()

# Created by Roy WAN at 21:02 29/12/2019 using PyCharm

import datetime
from time import sleep


def get_time():
    day20 = datetime.datetime.strptime('2020-12-27 0:0:0', '%Y-%m-%d %H:%M:%S')  # 设置未来时间
    now = datetime.datetime.today()
    delta = day20 - now  # delta存储两个时间的时间，差精确到毫秒
    day = delta.days  # 获取两个时间之间的天数
    hour = int(delta.seconds / 60 / 60)  # 使用int函数把小时取整
    minutes = int((delta.seconds - hour * 60 * 60) / 60)  # 使用int函数把分钟取整
    seconds = delta.seconds - hour * 60 * 60 - minutes * 60  # 使用int函数把秒取整
    return day, hour, minutes, seconds


n = 180
while n > 0:
    print("\r",
          '\033[31;43m距离zyx生日：\033[43m' + '\033[34;43m' + str(get_time()[0]) + '\033[43m' + '\033[31;43m天\033[43m' +
          '\033[34;43m' + str(get_time()[1]) + '\033[43m' + '\033[31;43m小时\033[43m' +
          '\033[34;43m' + str(get_time()[2]) + '\033[43m' + '\033[31;43m分钟\033[43m' +
          '\033[34;43m' + str(get_time()[3]) + '\033[43m' + '\033[31;43m秒\033[0m',
          end='')
    sleep(1)
    n -= 1
